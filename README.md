# Functional requirements:

- publishing posts with photos and text
- geolocation binding to posts
- viewing user publications
- subscription to users
- rating user posts
- commenting on posts
- generating feeds based on the travels of other users

# Non-functional requirements:

- DAU 10_000_000
- CIS audience
- desktop, mobile and web applications
- high availability 99.99%
- data is always stored
- on average one publication from DAU / 10
- on average 3 comments on post from one user
- on average 10 views of posts from one user
- on average 10 views of comments from one user
- storing the latest data of account on app
- limit of image size - 1 Mb
- limit of post and comment length - 1000 tokens
- high and low seasons (holidays, New Year, summer)

# Load assessment:

```
RPS_read = dau * avg_reads_per_day_by_user / 86 400 = (10_000_000 * 10) / 86_400 ≈ 1157.4

RPS_write_posts = dau * avg_write_posts_per_day_by_user / 86 400 = (10_000_000 * 1) / 86_400 ≈ 115.74

RPS_write_comments = dau * avg_write_comm_per_day_by_user / 86 400 = (10_000_000 * 3) / 86_400 ≈ 347.22
```

```
Traffic_read = RPS_read * avg_request_size = 1157.4 * (0.002 Mb * (post size + 10*(comments size)) + 1 Mb (image size)) ≈ 1_182.86 Mb/s

Traffic_write_posts = RPS_write_posts * avg_request_size = 115.74 * (0.002 Mb (post size)) ≈ 0.2315 Mb/s

Traffic_write_img = RPS_write_posts * avg_request_size = 115.74 * (1 Mb (image size)) ≈ 115.74 Mb/s

Traffic_write_comments = RPS_write_posts * avg_request_size = 347.22 * (0.002 Mb (comment size)) ≈ 0.694 Mb/s
```

# Disks:

```
Capacity_db =  Traffic (write_posts + write_comments) * 86_400 * 365 = (0.2315 + 0.694) * 86400 * 365 ≈ 29.19 Tb

Capacity_S3 =  Traffic (write_posts + write_comments) * 86_400 * 365 = 115.74 * 86400 * 365 = 3650 Tb
```

The type of disks is SSD, because users of the social network want to get information quickly.
We will use large disks (32 Tb) for S3 and small disks for DB (2 Tb).

## DB (posts and comments)

```
Disks_for_capacity = 29.19 ТБ / 2 ТБ ≈ 15

Disks_for_throughput = (1_182.86 + 0.2315  + 0.694) / 1000 МБ/с ≈ 1.18

Disks_for_iops = (0.2315 + 0.694) / 1000 ≈ 0.001

Disks = max(ceil(15), ceil(1.18), ceil(0.001)) = 15
```

## S3

```
Disks_for_capacity = 3650 ТБ / 32 ТБ ≈ 114

Disks_for_throughput = (1_182.86 + 115.74) / 1000 МБ/с ≈ 1.3

Disks_for_iops = 115.97 / 1000 ≈ 0.12

Disks = max(ceil(114), ceil(1.3), ceil(0.12)) = 114
```


# Replication and sharding

Replication factor is 2 (db) for increasing fault tolerance.
Type of replication - async multi-master replication.

## Sharding tables:

1. users - id
2. subscribers - subscribing_user_id
3. posts - id
4. likes - post_id
5. comments - post_id
6. media - post_id

# Hosts

## db

```
disks_per_host = 2

Hosts_db = disks / disks_per_host = 15 / 2  ≈ 8
Hosts_with_replication = hosts * replication_factor = 8 * 2 = 16
```

## S3

```
disks_per_host = 2

Hosts_db = disks / disks_per_host = 114 / 2  ≈ 57
Hosts_with_replication = hosts * replication_factor = 57 * 1 = 57
```