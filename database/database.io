// Use DBML to define your database structure
// Docs: https://dbml.dbdiagram.io/docs

Table subscribes {
  subscribing_user_id integer
  subscribed_user_id integer
  created_at timestamp 
}

Table users {
  id integer [primary key]
  username varchar
  firstname varchar
  lastname varchar
  birthdate date
  country varchar
  city varchar
  status integer
  created_at timestamp
}

Table posts {
  id integer [primary key]
  title varchar
  body text [note: 'Content of the post']
  user_id integer
  lat float
  long float
  status integer
  created_at timestamp
}

Table likes {
  id integer [primary key]
  post_id integer
  user_id integer
  status integer
  created_at timestamp
}

Table media {
  id integer [primary key]
  post_id integer
  media_url varchar
  type varchar
  status integer
  created_at timestamp
}

Table comments {
  id integer [primary key]
  post_id integer
  body text
  user_id integer
  status integer
  created_at timestamp
}

Ref: users.id < posts.user_id

Ref: users.id < subscribes.subscribing_user_id

Ref: users.id < subscribes.subscribed_user_id

Ref: posts.id < comments.post_id

Ref: posts.id < likes.post_id

Ref: posts.id < media.post_id

Ref: users.id < likes.user_id

Ref: users.id < comments.user_id
